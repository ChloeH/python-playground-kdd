#!/usr/bin/python3

import os
import sys
import nltk
from custompackages.input_validation_ceh import *


repeat = True  # repeat entire process for another file
input_types = ['raw string', 'text file']
num_input_types = len(input_types)
user_input = str()
input_type = int()
is_relative_path = False
input_path = str()
data_raw = str()
data_word_tokens = list()
data_tagged_pos = list()
data_sentences = list()
temp_sent = list()
curr_index = int()
data_num_sentences = int()
grammar = r"""
    NP: {<([ACJNP]|DT).*>+<(N.*|VBG)>+}
        {(<(DT|A).*>|<POS>|<PP\$>)<[CIDJNP].*>+<(N.*|VBG)>+}
        {(<(DT|A).*>|<POS>|<PP\$>)?<[CDJNP].*>+<(N.*|VBG)>+}
        {<N.*>+<IN>+<(N.*|VBG)>+}
"""
np_chunk_parser = nltk.RegexpParser(grammar)


print('~~ Parts of Speech (PoS) Tagger and Noun Phrase (NP) Chunker ~~')

while repeat:
    print('\nCurrently accepted input types:')

    for i in range(num_input_types):
        print('(', (i + 1), ')  ', input_types[i], sep='')

    data_sentences = []  # empty the list in case we're repeating
    data_raw = ''        # empty the string in case we're repeating

    user_input = validate_input('\nChoose input type: ',
                                'Error: invalid input type.',
                                tuple(
                                    str(x + 1) for x in range(num_input_types))
                                )
    input_type = int(user_input)

    print(input_types[(input_type - 1)], 'it is', end='\n\n')

    if input_type == 1:   # raw string
        print('Type or paste the string below.', 
              'Enter a blank line to terminate input.')
        user_input = input('Input string: ')

        while user_input != '':
            data_raw += user_input + '\n'
            user_input = input('...  ')

    elif input_type == 2:  # text file
        while True:
            user_input = validate_input(
                'Choose (a)bsolute or (r)elative path: ',
                'Error: invalid path type.',
                ('a', 'absolute', 'r', 'relative'))

            is_relative_path = (user_input[0] == 'r')

            if is_relative_path:
                user_input = input('Enter the relative path: ')
                input_path = os.path.join(sys.path[0], user_input)
            else:
                input_path = input('Enter the absolute path: ')

            if os.path.exists(input_path):
                break
            else:
                print("'", input_path, "' is not a valid path.", sep='')

        with open(input_path, 'r') as text_file:
            data_raw = text_file.read()

    print('\nUpcoming prompts (in order): ', 
          ' - whether to strip line breaks',
          ' - whether to print the raw data',
          ' - whether to print the word-tag pairs one sentence at a time',
          ' - whether to print the noun phrase chunks one sentence at a time', 
          'Consider yourself warned.',
          sep='\n',
          end='\n\n')

    if yn_prompt('Would you like to '
                 'remove line breaks from the input? (Y/N): '):
        data_raw.replace('\n', '')

    if yn_prompt('Would you like to see the raw data? (Y/N): '):
        print('\nRaw data:', data_raw, sep='\n')

        if input_type == 2:  # text file
            print()  # purely for formatting reasons

    data_word_tokens = nltk.word_tokenize(data_raw)

    data_tagged_pos = nltk.pos_tag(data_word_tokens)

    for (word, tag) in data_tagged_pos:
        temp_sent.append((word, tag))

        if tag == '.':
            data_sentences.append(temp_sent)
            temp_sent = []

    data_num_sentences = len(data_sentences)

    if yn_prompt('Would you like to '
                 'see the tagged words one sentence at a time? (Y/N): '):
        print('\nSentences:')

        curr_index = 0

        while curr_index < data_num_sentences:
            print('\n', data_sentences[curr_index], sep='')

            user_input = validate_input(
                '\n<enter> to see more, <S> to skip the rest: ',
                'Error: invalid input.',
                ('', 's', 'skip'))

            if user_input == '':
                curr_index += 1
            else:
                curr_index = data_num_sentences
                print('Skipped', end='\n\n')

    if yn_prompt('Would you like to '
                 'see the noun phrase chunks one sentence at a time? (Y/N): '):
        print('\nNoun Phrase Chunks:')

        curr_index = 0

        while curr_index < data_num_sentences:
            print('\n', np_chunk_parser.parse(data_sentences[curr_index]),
                  sep='')

            user_input = validate_input(
                '\n<enter> to see more, <S> to skip the rest: ',
                'Error: invalid input.',
                ('', 's', 'skip'))

            if user_input == '':
                curr_index += 1
            else:
                curr_index = data_num_sentences
                print('Skipped', end='\n\n')

    repeat = yn_prompt('Would you like to enter new data? (Y/N): ')

print('Goodbye!')
