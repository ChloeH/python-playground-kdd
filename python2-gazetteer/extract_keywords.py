import os
import sys
import csv
import time


# Import the keywords ('complete_gazetteer.txt' contains ALL of them)

f = open(os.path.join(sys.path[0], 'gazetteer.txt'), 'r')
all_gazetteer_keywords = f.read().lower().split('\n')
f.close()


'''
# Import the texts you want to search (DEPRECATED)

f = open(os.path.join(sys.path[0], 'texts.txt'), 'r')
all_texts = f.read().lower().split('\n')
f.close()
'''


# Initialize data lists and I/O file names

all_texts = []
full_row = []
time_str = time.strftime('%m-%d-%Y-(%H-%M-%S)')
dataset_file_path = os.path.join(sys.path[0],
                                 'The_Dataset_-_Alumni_Oxonienses-Jas1.csv')
dataset_new_file_path = os.path.join(sys.path[0],
                                     'output_' +
                                     str(time_str) +
                                     '.csv')

# Import the 'Details' column from the CSV file

with open(dataset_file_path) as csv_file:
    reader = csv.DictReader(csv_file)

    # the full row for each entry, which will be used to recreate the improved
    # CSV file in a moment (?)

    for row in reader:
        full_row.append((row['Name'],
                         row['Details'],
                         row['Matriculation Year']))

        # the column we want to parse for our keywords

        row = row['Details'].lower()
        all_texts.append(row)


row_counter = 0


with open(dataset_new_file_path, 'a') as csv_file:

    # define the column headers and write them to the new file

    field_names = ['Name', 'Details', 'Matriculation Year', 'Placename']
    writer = csv.DictWriter(csv_file, fieldnames=field_names)
    writer.writeheader()

    # define the output for each row and then print to the output CSV

    writer = csv.writer(csv_file)

    for entry in all_texts:
        matches = 0
        stored_matches = []
        all_words = entry.split(' ')

        for words in all_words:

            # remove punctuation that will interfere with matching

            words = words.replace(',', '')
            words = words.replace('.', '')
            words = words.replace(';', '')

            # record all keyword matches that are found

            if words in all_gazetteer_keywords:
                matches += 1

                if words not in stored_matches:
                    stored_matches.append(words)

        match_string = ''

        # update data for output files with info about matches

        if matches != 0:
            for matches in stored_matches:
                match_string += matches + '\t'

            match_tuple = tuple(stored_matches)
            new_row = full_row[row_counter] + match_tuple
        else:
            new_row = full_row[row_counter]

        # write the result(s) for each row to the new CSV file
        writer.writerows([new_row])
        row_counter += 1


# Write all keyword matches to a text file

output_file_path = os.path.join(sys.path[0],
                                'keyword_matches_' +
                                str(time_str) +
                                '.txt')

f = open(output_file_path, 'a')
f.write(match_string + '\n')
f.close()
