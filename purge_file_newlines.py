#!/usr/bin/python3

import os
import sys
from datetime import datetime


file_name = input('Enter the relative path to the text file: ')
input_file_path = os.path.join(sys.path[0], file_name)
valid_input = os.path.exists(input_file_path)


while not valid_input:
    print("'", file_name, "' is not a valid path.", sep='')
    file_name = input('\nEnter the relative path to the text file: ')
    input_file_path = os.path.join(sys.path[0], file_name)
    valid_input = os.path.exists(input_file_path)


print('\n', 'You may leave this field blank.', sep='')
new_file_name = input("Enter a name for the output file: ")


if new_file_name == '':
    time_str = '{:_%m-%d-%Y-(%H-%M-%S)}'.format(datetime.now())
    end_index = file_name.rfind('.')
    new_file_name = file_name[:end_index] + time_str + '.txt'


output_file_path = os.path.join(sys.path[0], new_file_name)


with open(input_file_path, 'r') as input_file:
    file_contents = input_file.read().splitlines()
    clean_file_contents = ' '.join(file_contents)


with open(output_file_path, 'w') as output_file:
    output_file.write(clean_file_contents)


print(output_file_path)
