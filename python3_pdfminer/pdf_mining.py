#!/usr/bin/python3

# Source:
# https://github.com/dpapathanasiou/pdfminer-layout-scanner

import sys
import os
from binascii import b2a_hex

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument, PDFNoOutlines
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTFigure, LTImage


def with_pdf(pdf_doc, fn, pdf_pwd, *args):
    """
    Opens the PDF document, applies the given function to it, and
    returns the result.

    Given the name of a PDF document, its password (if needed), and a
    function meant to be applied to the opened PDF, return the output
    of the given function.

    :param pdf_doc: The name of a PDF document
    :type pdf_doc: str
    :param fn: A function to apply to the opened PDF
    :type fn: function
    :param pdf_pwd: The password for the PDF
    :type pdf_pwd: str
    :param args: Arbitrary positional arguments to be passed to the
        given function
    :type args: tuple
    :return: The return value for the given function or None if the
        PDF document cannot be parsed for any reason
    """
    result = None

    try:
        # open the pdf file
        fp = open(pdf_doc, 'rb')

        # create a parser object associated with the file object
        parser = PDFParser(fp)

        # create a PDFDocument object that stores the document structure
        doc = PDFDocument(parser)

        # connect the parser and document objects
        parser.set_document(doc)

        # supply the password for initialization
        doc.initialize(pdf_pwd)

        if doc.is_extractable:
            # apply the function and return the result
            result = fn(doc, *args)

        # close the pdf file
        fp.close()

    except IOError:  # the file doesn't exist or similar problem
        pass

    return result


def _parse_toc(doc):
    """
    Extracts and returns the table of contents data from a given open
    PDF document.

    This would typically be passed to a higher-order function that
    would take care of opening the PDF document before calling this
    function.

    :param doc: An open PDF document
    :type doc: PDFDocument
    :return: Extracted table of contents data
    :rtype: list
    """
    toc = []

    try:
        outlines = doc.get_outlines()

        for (level, title, dest, a, se) in outlines:
            toc.append((level, title))

    except PDFNoOutlines:  # can be thrown by 'get_outlines()'
        pass

    return toc


def get_toc(pdf_doc, pdf_pwd=''):
    """
    Extracts and returns the table of contents for a given PDF
    document, if the document has a table of contents.

    :param pdf_doc: The name of a PDF document
    :type pdf_doc: str
    :param pdf_pwd: The password for the PDF document
    :type pdf_pwd: str
    :return: The extracted table of contents, if it exists
    :rtype: list
    """
    return with_pdf(pdf_doc, _parse_toc, pdf_pwd)


def _parse_pages(doc, images_folder):
    """
    Extracts and returns the pages in a given open PDF document.

    This would typically be passed to a higher-order function that
    would take care of opening the PDF document before calling this
    function.

    :param doc: An open PDF document
    :type doc: PDFDocument
    :param images_folder: Path to directory where all extracted images
        should be saved
    :type images_folder: str
    :return: The text extracted from the pages
    :rtype: list(str)
    """
    rsrcmgr = PDFResourceManager()
    laparams = LAParams()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    # list of strings where each string represents the data extracted from a
    # particular page in the PDF document
    text_content = []

    for i, page in enumerate(PDFPage.create_pages(doc)):
        interpreter.process_page(page)

        # receive the LTPage object for this page
        layout = device.get_result()

        # layout is an LTPage object which may contain child objects
        # like LTTextBox, LTFigure, LTImage, etc.
        text_content.append(parse_lt_objs(layout, (i + 1), images_folder))

    return text_content


def get_pages(pdf_doc, pdf_pwd='', images_folder='/tmp'):
    """
    Processes each of the pages in a given PDF document and returns a
    list of strings representing the text found on each page.

    :param pdf_doc: The name of a PDF document
    :type pdf_doc: str
    :param pdf_pwd: The password for the PDF document
    :type pdf_pwd: str
    :param images_folder: Path to directory where all extracted images
        should be saved
    :type images_folder: str
    :return: The text extracted from the pages
    :rtype: list(str)
    """
    return with_pdf(pdf_doc, _parse_pages, pdf_pwd, *tuple([images_folder]))


# TODO: expand 'parse_lt_objs' to include more LT* objects
# (author said they ignored several types)
# TODO: figure out what 'text' parameter is for
def parse_lt_objs(lt_objs, page_number, images_folder, text=[]):
    """
    Iterates through the given list of LT* objects and attempts to
    extract all text and image data contained in them. The resulting
    string representation of the data on the page is then returned.

    Images are represented by an HTML-style <img /> tag in the
    returned string, which includes a path to the directory where all
    successfully extracted images are saved. Note that this is a very
    basic function, if only in the sense that a number of common LT*
    objects are ignored when parsing the page.

    :param lt_objs: Layout objects found on a particular page of a PDF
        document
    :type lt_objs: LT*
    :param page_number: The number of the page of the PDF document
        that contains the given LT* objects
    :type page_number: int
    :param images_folder: Path to directory where all extracted images
        should be saved
    :type images_folder: str
    :param text: ???
    :type text: list
    :return: A string representation of the data extracted from the
        page
    :rtype: str
    """
    text_content = []

    page_text = {}
    # key = (x0, x1) of the bounding box (bbox),
    # value = list of text strings within that bbox width (physical column)

    for lt_obj in lt_objs:
        if isinstance(lt_obj, LTTextBox) or \
           isinstance(lt_obj, LTTextLine):  # text
            # arrange it logically based on its column width
            page_text = update_page_text_hash(page_text, lt_obj)

        elif isinstance(lt_obj, LTImage):  # image
            # save it to the designated folder, and note its place in the text
            saved_file = save_image(lt_obj, page_number, images_folder)

            if saved_file:
                # use html style <img /> tag to mark
                # the position of the image within the text
                text_content.append('<img src="' +
                                    os.path.join(images_folder,
                                                 saved_file) +
                                    '" />')
            else:
                print(sys.stderr, "error saving image on page",
                      page_number, repr(lt_obj))

        elif isinstance(lt_obj, LTFigure):  # LTFigure objects are containers
                                            # for other LT* objects
            # recurse through the children
            text_content.append(parse_lt_objs(lt_obj,
                                              page_number,
                                              images_folder,
                                              text_content))
            # TODO: figure out what this last arg is for

    for (_, value) in sorted(page_text.items()):
        # sort the page_text hash by the keys (x0,x1 values of the bbox),
        # which produces a top-down, left-to-right sequence of related columns
        text_content.append(''.join(value))

    return '\n'.join(text_content)


def update_page_text_hash(h, lt_obj, pct=0.2):
    """
    Uses the position and size of the given layout object as well as
    the given percentage to try to create lists of associated texts. It
    is assumed that the text within the document being parsed is
    grouped by columns.

    Each LT* object provides a bounding box (bbox) attribute, which is
    a four-item tuple that describes the object's position on the page
    as follows:
    lt_obj.bbox : (x0, y0, x1, y1)
    x0 : points between the left side of the page and the left side of
         the object - left-padding
    y0 : points between the bottom of the page and the bottom of the
         object
    x1 : points between the left side of the page and the right side of
         the object
    y1 : points between the bottom of the page and the top of the
         object

    The only information that is used is x0 and x1. The idea is to try
    to logically group all of the text in a document into coherent
    chunks based on the arrangement of the text on the pages. To this
    end, the given dictionary maps tuples of the form (x0, x1) to lists
    of strings where each string came from a layout object that was
    determined to belong to the column described by (x0, x1). The text
    contained in the given layout object is either appended to an
    existing entry or added as a new one, and the dictionary is
    returned.

    The given percent is used when determining if the text contained in
    a given layout object should be added to an existing entry or not.
    More specifically, if the x0 and x1 values of the given layout
    object are within the given percent of an existing key's x0 and x1
    values, then the text contained within the object is appended to
    the list of texts associated with that key. Otherwise, a new entry
    is added to the given dictionary that maps (x0, x1) for the given
    layout object to a list whose first and only item is the given
    layout object's text.

    :param h: A dictionary mapping the horizontal positioning of
        columns found in a document to lists of associated texts that
        were extracted from layout objects that were determined to
        belong to the same group
    :type h: dict of tuple(int, int): list(str)
    :param lt_obj: A layout object that contains text
    :type lt_obj: LT*
    :param pct: Percentage that the horizontal positioning of the given
        layout object can differ from those of an existing mapping
        before it is considered a new column
    :type pct: float
    :return: The given dictionary, but now with the text contained in
        the given layout object added to a new or preexisting entry,
        depending on which one made the most sense based on the
        grouping scheme
    :rtype: dict of tuple(int, int): list(str)
    """

    x0 = lt_obj.bbox[0]  # pts between left side of page and left side of obj
    x1 = lt_obj.bbox[2]  # pts between left side of page and right side of obj

    key_found = False

    for (k, v) in h.items():
        hash_x0 = k[0]

        if (hash_x0 * (1.0 + pct)) >= x0 >= (hash_x0 * (1.0 - pct)):
            hash_x1 = k[1]

            if (hash_x1 * (1.0 + pct)) >= x1 >= (hash_x1 * (1.0 - pct)):
                # the text inside this LT* object was positioned at about the
                # same width as a prior series of text, so they belong together
                key_found = True
                v.append(to_bytestring(lt_obj.get_text()))
                h[k] = v

    if not key_found:
        # based on the width of the object, the given text is a new series,
        # so it gets its own series (entry in the hash)
        h[(x0, x1)] = [to_bytestring(lt_obj.get_text())]

    return h


def save_image(lt_image, page_number, images_folder):
    """
    Tries to save the image data from the given LTImage object and
    returns the file name if successful.

    Note that PDFMiner can only seem to find JPGs as LTImage objects.

    :param lt_image: A layout object containing image data
    :type lt_image: LTimage
    :param page_number: The number of the page of the PDF document
        that contains the given image
    :type page_number: int
    :param images_folder: Path to directory where the extracted image
        should be saved
    :type images_folder: str
    :return: The name of the saved image file or None if the file was
        not successfully saved
    :rtype: str
    """
    result = None  # in case the image is not successfully saved

    if lt_image.stream:
        file_stream = lt_image.stream.get_rawdata()

        if file_stream:
            file_ext = determine_image_type(file_stream[0:4])

            if file_ext:
                file_name = ''.join([str(page_number), '_',
                                     lt_image.name, file_ext])

                if write_file(images_folder, file_name,
                              file_stream, flags='wb'):
                    result = file_name

    return result


def determine_image_type(stream_first_4_bytes):
    """
    Use magic numbers to try to identify the file type based on the
    given stream of the first four bytes of the file.

    :param stream_first_4_bytes: The first four bytes of an image file
    :type stream_first_4_bytes: bytearray
    :return: The extension for the file, including the period
    :rtype: str
    """
    file_type = None

    bytes_as_hex = b2a_hex(stream_first_4_bytes)

    if bytes_as_hex.startswith('ffd8'):
        file_type = '.jpeg'

    elif bytes_as_hex == '89504e47':
        file_type = '.png'

    elif bytes_as_hex == '47494638':
        file_type = '.gif'

    elif bytes_as_hex.startswith('424d'):
        file_type = '.bmp'

    #  TODO: add a default case that throws an error or something

    return file_type


def write_file(folder, filename, filedata, flags='w'):
    """
    Writes the file data to the disk using the given folder path and
    file name and returns a boolean indicating whether or not the file
    was successfully saved.

    No attempt will be made to write the file to the disk if the
    directory described by the given path does not exist.

    :param folder: The absolute path to the folder that the file should
        be written to
    :type folder: str
    :param filename: The name that should be used when saving the file
    :type filename: str
    :param filedata: The data to be written when saving the file
    :param flags: The flags that should be used when saving the file
        w  - write text
        wb - write binary
        a  - use instead of 'w' for append
    :type flags: str
    :return: Whether the file was successfully written
    :rtype: bool
    """
    result = False

    if os.path.isdir(folder):
        try:
            file_obj = open(os.path.join(folder, filename), flags)
            file_obj.write(filedata)
            file_obj.close()

            result = True

        except IOError:
            pass

    return result


def to_bytestring(s, enc='utf-8'):
    """
    Converts the given unicode string to a bytestring using the
    standard encoding. If the string is already a bytestring, it is
    returned as is.

    This circumvents an error that is thrown if any non-English
    characters (Unicode characters beyond 128) are encountered while
    parsing a page from a PDF document.

    :param s: A given unicode string
    :type s: str
    :param enc: The encoding to use on the given string
    :type enc: str
    :return: The encoded bytestring
    :rtype: str
    """
    if s:
        if isinstance(s, str):
            return s
        else:
            return s.encode(enc)
