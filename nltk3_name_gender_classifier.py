import nltk
import random
from nltk.corpus import names
from nltk.classify import apply_features
from custompackages.input_validation_ceh import *

def gender_features(name):
    """
    Encodes informative features of words.

    :param name: A name
    :type name: str
    :return: The encoded features for the name
    :rtype: dict
    """

    name = name.lower()

    num_vowels = 0
    num_consonants = 0

    for c in name:
        if c in 'aeiouy':
            num_vowels += 1
        else:
            num_consonants += 1

    return {'last_letter': name[-1], 
            'last_two_letters': name[-2:], 
            'first_two_letters': name[:2], 
            'vowel_cons_ratio': (num_vowels / num_consonants)}


print('\nCompiling set of names...')
labeled_names = ([(name, 'male') for name in names.words('male.txt')] +
                 [(name, 'female') for name in names.words('female.txt')])

random.shuffle(labeled_names)

print('Extracting gender features...')
feature_sets = [(gender_features(name),
                 gender) for (name, gender) in labeled_names]

set_size = len(labeled_names)
print('\nTotal size of names set: ', set_size)


valid_input = False  # flag as false to ensure we enter the validation loop

while not valid_input:
    user_input = input('How many names would you like to use for training? ')
    train_size, valid_input = try_parse_int(user_input, 1, set_size - 1)


print('Building test set and training set...')
train_set = apply_features(gender_features, labeled_names[:train_size])
test_set = apply_features(gender_features, labeled_names[train_size:])


print('Training gender classifier...\n')
classifier = nltk.NaiveBayesClassifier.train(train_set)

print('Classifier accuracy: {:.2f}'.format(nltk.classify.accuracy(classifier,
                                                                  test_set)))


valid_input = False  # flag as false to ensure we enter the validation loop

while not valid_input:
    user_input = input('\n'
                       'How many informative features would you like to see? ')
    user_input, valid_input = try_parse_int(user_input)

if user_input:
    print(classifier.show_most_informative_features(user_input))


print('\n{:^10}'.format('User Testing'))

user_input = input("\nEnter a name or leave blank to quit: ").lower()
num_correct = 0
total = 0

while user_input:
    total += 1

    gender_guess = classifier.classify(gender_features(user_input))
    print('Gender guess: ', gender_guess)
    correct_gender = validate_input('Correct gender: ',
                                    'Error: invalid gender.',
                                    ('f', 'female', 'm', 'male'))

    # TODO: keep training the model from input
    if gender_guess[0] == correct_gender[0]:
        num_correct += 1

    print('\nCurrent cumulative accuracy: {:.2%}'.format(num_correct / total))

    user_input = input("\nEnter a name or leave blank to quit: ").lower()

if total:
    print('\nFinal cumulative accuracy: {:.2f}'.format(num_correct / total))
    print(num_correct, 'correct out of', total, 'total')

print('\nGoodbye!')
