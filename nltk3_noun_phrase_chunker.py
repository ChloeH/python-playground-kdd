#!/usr/bin/python3

import nltk, re, pprint
import sys
from custompackages.input_validation_ceh import *


# these rules are a work in progress
grammar = r"""
    NP: {<([ACJNP]|DT).*>+<(N.*|VBG)>+}
        {(<(DT|A).*>|<POS>|<PP\$>)<[CIDJNP].*>+<(N.*|VBG)>+}
        {(<(DT|A).*>|<POS>|<PP\$>)?<[CDJNP].*>+<(N.*|VBG)>+}
        {<N.*>+<IN>+<(N.*|VBG)>+}
"""

# Hard coded, manually tagged sentences below

"""
sentences = [
    [("the", "DT"), ("little", "JJ"), ("yellow", "JJ"), ("dog", "NN"),
        ("barked", "VBD"), ("at", "IN"),  ("the", "DT"), ("cat", "NN")],
    [("another", "DT"), ("sharp", "JJ"), ("dive", "NN")], 
    [("trade", "NN"), ("figures", "NNS")], 
    [("any", "DT"), ("new", "JJ"), ("policy", "NN"), ("measures", "NNS")], 
    [("earlier", "JJR"), ("stages", "NNS")], 
    [("Panamanian", "JJ"), ("dictator", "NN"), ("Manuel", "NNP"),
        ("Noriega", "NNP")],
    [("his", "PRP$"), ("Mansion", "NNP"), ("House", "NNP"), ("speech", "NN")], 
    [("the", "DT"), ("price", "NN"), ("cutting", "VBG")], 
    [("3", "CD"), ("%", "NN"), ("to", "TO"), ("4", "CD"), ("%", "NN")], 
    [("more", "JJR"), ("than", "IN"), ("10", "CD"), ("%", "NN")], 
    [("the", "DT"), ("fastest", "JJS"), ("developing", "VBG"),
        ("trends", "NNS")],
    [("'s", "POS"), ("skill", "NN")]
]
"""

cp = nltk.RegexpParser(grammar)
sentences, temp_sent = [], []

print("\nGetting tagged words...")
tagged_words = nltk.corpus.brown.tagged_words()

tagged_words_length, curr_index = len(tagged_words), 0

print("Tokenizing sentences...", end='\n')

for (word, tag) in tagged_words:
    temp_sent.append((word, tag))
    curr_index += 1

    if tag == '.':
        sentences.append(temp_sent)
        temp_sent = []

        # display percent progress in a super stylin' way

        prog_perc = curr_index / tagged_words_length
        prog_str = '{:.2%}'.format(prog_perc)
        print('\rProgress: ', prog_str, sep='', end='')

print(end='\n\n')
valid_input = True
curr_index, sentences_length = 0, len(sentences)

while curr_index < sentences_length:
    print(cp.parse(sentences[curr_index]))
    user_input = validate_input('\n<enter> to see more, <Q> to quit: ',
                                'Error: invalid input',
                                ('', 'q', 'quit'))

    if user_input == '':
        curr_index += 1
    else:
        curr_index = sentences_length

print('Goodbye!')
