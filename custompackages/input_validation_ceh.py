#!/usr/bin/python3

import sys
import os
from datetime import datetime


def get_choice(options, greeting='Options:',):
    """
    Given a list of all of the options, print a greeting and a numbered
    list of the given options, then prompt the user to pick from the
    list until a valid selection is received. Return the selection.

    :param options: A list of the options
    :type options: list(str)
    :param greeting: The message to be printed before the list of
        options
    :type greeting: str
    :return: A valid selection
    :rtype: int
    """
    num_options = len(options)

    print(greeting)

    for i in range(num_options):
        print('(', (i + 1), ')  ', options[i], sep='')

    u_in = validate_input('\nInput choice: ',
                          'Error: invalid input type.',
                          tuple(str(x + 1) for x in range(num_options)))

    choice = int(u_in) - 1

    # optional print
    print("Selection: ", u_in, " (", options[choice], ")",
          sep='', end='\n\n')

    return choice


def try_parse_int(u_in, lb=0, ub=999):
    """
    Attempt to parse and validate integer input.

    Given a string representing input and a range that the input should
    fall between, attempt to parse the input string and return a tuple
    containing the input (as an int if parsed, as a string otherwise)
    and a boolean indicating the validity of the input. For invalid
    input, print an error message identifying the problem.

    :param u_in: Input from the user
    :type u_in: str
    :param lb: The lower bound for the integer, inclusive 
        (defaults to 0)
    :type lb: int
    :param ub: The upper bound for the integer, inclusive 
        (defaults to 999)
    :type ub: int
    :return: The input (int if valid, str otherwise) and whether the 
        input is valid
    :rtype: tuple(str or int, bool)
    """

    try:
        u_in = int(u_in)
        valid = (lb <= u_in <= ub)
    except ValueError:
        print('Error: input must be an integer.', end='\n\n')
        return False

    if not valid:
        print('Error: input must be between ', lb, ' and ', ub,
              ', inclusive.', sep='', end='\n\n')

    return u_in, valid


def is_valid_input(u_in, valid_in=('y', 'n'), case_sensitive=False):
    """
    Determine the validity of the given string input.

    Given a string representing the input, a tuple containing all valid
    values, and a boolean indicating whether or not the input is case
    sensitive, return a boolean representing the validity of the input.

    :param u_in: Input from the user
    :type u_in: str
    :param valid_in: All valid input
    :type valid_in: tuple(str)
    :param case_sensitive: Whether the input is case sensitive
    :type case_sensitive: bool
    :return: Whether the given input is valid
    :rtype: bool
    """

    if not case_sensitive:
        u_in = u_in.lower()
        valid_in = [inp.lower() for inp in valid_in]

    return u_in in valid_in


def yn_prompt(prompt):
    """
    Validate and return a boolean representing the user's answer to a
    given 'yes or no'-type question.

    Given a string representing the prompt message, show the message to
    the user until they answer either 'yes' or 'no.' Return a boolean
    representing their answer.

    :param prompt: Prompt message
    :type prompt: str
    :return: Whether the user answered 'yes'
    :rtype: bool
    """
    u_in = input(prompt)[0]

    while not is_valid_input(u_in):
        u_in = input(prompt)[0]

    return u_in == 'y'


def validate_input(prompt, error_message='Error: invalid input',
                   valid_in=('y', 'n'), case_sensitive=False):
    """
    Print given prompt and obtain a valid response.

    Given a string representing the desired prompt message, a string
    containing the desired error message for invalid input, a tuple
    containing valid input values, and a boolean indicating whether
    or not the input is case sensitive, prompt the user for input until
    a valid response is received and then return that response. For
    invalid input, print the given error message identifying the
    problem before displaying the given prompt message again.

    :param prompt: Prompt message
    :type prompt: str
    :param error_message: Error message for invalid input
    :type error_message: str
    :param valid_in: All valid input
    :type valid_in: tuple(str)
    :param case_sensitive: Whether the input is case sensitive
    :type case_sensitive: bool
    :return: Valid user input
    :rtype: str
    """
    u_in = input(prompt)

    while not is_valid_input(u_in, valid_in, case_sensitive):
        print(error_message, end='\n\n')
        u_in = input(prompt)

    return u_in


def get_valid_path(alt_path_prompt='', is_directory=False, create_if_dne=False,
                   default_path='output', default_path_is_relative=True):
    """
    Given a boolean indicating whether the path should be to a file or
    directory, prompt the user to input a path until a valid one is
    received, then return it. A default prompt will be displayed when
    asking for a path unless an alternate prompt is given.

    Before asking the user to input the path, prompt them to indicate
    if the path is relative or absolute, or if they would like to use a
    default path. If the user inputs an invalid path, display an error
    message and repeat all of the prompts until a valid path is
    received.

    :param alt_path_prompt: Alternate prompt to display, if provided
        (optional)
    :type alt_path_prompt: str
    :param is_directory: Whether the returned path is to a directory or
        to a file
    :type is_directory: bool
    :param create_if_dne: Whether to create the input file or directory
        if it does not already exist
    :type create_if_dne: bool
    :param default_path: A valid path to a directory
    :type default_path: str
    :param default_path_is_relative: Whether the given default path is
        relative
    :type default_path_is_relative: bool
    :return: A valid path to an existing directory or file
    :rtype: str
    """
    path_prompt = alt_path_prompt

    if default_path_is_relative:
        default_path = os.path.join(sys.path[0], default_path)

    while True:
        u_in = validate_input(
            'Choose (a)bsolute, (r)elative, or (d)efault path: ',
            'Error: invalid path type.',
            ('a', 'absolute', 'r', 'relative', 'd', 'default'))

        if u_in[0] == 'd':  # user chose default path
            u_in_path = default_path
        else:
            is_relative_path = (u_in[0] == 'r')

            if alt_path_prompt == '':
                path_prompt = \
                    'Enter the ' + \
                    ('relative' if is_relative_path else 'absolute') + \
                    ' path to the ' + \
                    ('directory' if is_directory else 'file') + \
                    ': '

            u_in_path = input(path_prompt)

            if is_relative_path:
                u_in_path = os.path.join(sys.path[0], u_in_path)

        if (is_directory and os.path.isdir(u_in_path)) or \
           (not is_directory and os.path.exists(u_in_path)):
            break

        elif create_if_dne:
            try:
                if is_directory:
                    os.makedirs(u_in_path)
                else:
                    open(u_in_path, 'a').close()  # kind of like 'touch'

                print("'", u_in_path, "' successfully created.", sep='')

                break
            except:
                print("Failed to create '", u_in_path, "'\n",
                      sys.exc_info()[0], sep='')

        else:
            print("'", u_in_path, "' is not a valid ",
                  "directory" if is_directory else "file",
                  " path.", sep='')

    return u_in_path


def generate_valid_output_file_name(prefix='output', timestamp=False,
                                    ext='.txt', default_path='output',
                                    default_path_is_relative=True):
    """
    Given a prefix, an extension, and a boolean indicating whether a
    timestamp should be appended to the file name, prompt the user for
    a valid directory and return a full, valid path for an output file.

    :param prefix: The main part of the output file name, which appears
        before the timestamp if one is used
    :type prefix: str
    :param timestamp: Whether a timestamp should be appended to the
        output file's name
    :type timestamp: bool
    :param ext: The extension for the output file, including the period
    :type ext: str
    :param default_path: A valid path to a directory
    :type default_path: str
    :param default_path_is_relative: Whether the given default path is
        relative
    :type default_path_is_relative: bool
    :return: A valid path to an output file
    :rtype: str
    """
    complete = False

    while not complete:
        out_path = get_valid_path(
            'Choose a directory for the output file: ', is_directory=True,
            create_if_dne=True, default_path=default_path,
            default_path_is_relative=default_path_is_relative)

        if timestamp:
            out_name = prefix + \
                       '{:_%m-%d-%Y-(%H-%M-%S)}'.format(datetime.now()) + \
                       ext
        else:
            out_name = prefix + ext

        out_path_full = os.path.join(out_path, out_name)
        complete = not os.path.exists(out_path_full)

        while os.path.exists(out_path_full):
            print("'", out_path_full, "' already exists.", sep='')

            if yn_prompt('Would you like to add a timestamp '
                         'to the output file name? '):
                out_name = prefix + \
                           '{:_%m-%d-%Y-(%H-%M-%S)}'.format(datetime.now()) + \
                           ext
                out_path_full = os.path.join(out_path, out_name)
                complete = True

            elif yn_prompt('Would you like to choose '
                           'a different location?'):
                complete = False
                break
            else:
                print('The existing file will be affected.')
                complete = True
                break

    return out_path_full
