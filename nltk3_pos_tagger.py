#!/usr/bin/python3

import os
import nltk
from custompackages.input_validation_ceh import *


repeat = True  # repeat entire process for another file
input_types = ['raw string', 'text file']
valid_input = False
user_input = str()
input_type = int()
data_raw = str()
data_word_tokens = list()
data_tagged_pos = list()
data_sentences = list()
temp_sent = list()
curr_index = int()
data_num_sentences = int()

print('~~ Parts of Speech (PoS) Tagger ~~')

while repeat:
    print('\nCurrently accepted input types:')

    for i in range(len(input_types)):
        print('(', (i + 1), ')  ', input_types[i], sep='')

        if i == len(input_types):
            print()

    valid_input = False  # set to use for validating input type choice
    data_sentences = []  # empty the list in case we're repeating
    data_raw = ''        # empty the string in case we're repeating

    while not valid_input:
        user_input = input('\nChoose input type: ')
        input_type, valid_input = try_parse_int(user_input, 1, len(input_types))

    print(input_types[(input_type - 1)], 'it is', end='\n\n')
    valid_input = False  # reset to use for validating data input

    if input_type == 1:   # raw string
        print('Type or paste the string below.', 
              'Enter a blank line to terminate input.')
        user_input = input('Input string: ')

        while user_input != '':
            data_raw += user_input + '\n'
            user_input = input('...  ')

    elif input_type == 2:  # text file
        print('')
        user_input = input('Enter the absolute path to the text file: ')
        valid_input = os.path.exists(user_input)

        while not valid_input:
            print("'", user_input, "' is not a valid path.", sep='')
            user_input = input('\nEnter the absolute path to the text file: ')
            valid_input = os.path.exists(user_input)

        with open(user_input, 'r') as text_file:
            data_raw = text_file.read()

    print('\nUpcoming prompts (in order): ', 
          ' - whether to strip line breaks',
          ' - whether to print the raw data',
          ' - whether to print the list of words from the data',
          ' - whether to print the list of word-tag pairs',
          ' - whether to print the word-tag pairs one sentence at a time',
          'Consider yourself warned.',
          sep='\n',
          end='\n\n')

    if yn_prompt('Would you like to '
                 'remove line breaks from the input? (Y/N): '):
        data_raw.replace('\n', '')

    if yn_prompt('Would you like to see the raw data? (Y/N): '):
        print('\nRaw data:', data_raw, sep='\n')

        if input_type == 2:  # text file
            print()  # purely for formatting reasons

    data_word_tokens = nltk.word_tokenize(data_raw)

    if yn_prompt('Would you like to see the word tokens? (Y/N): '):
        print('\nWord tokens:', data_word_tokens, sep='\n', end='\n\n')

    data_tagged_pos = nltk.pos_tag(data_word_tokens)

    if yn_prompt('Would you like to see the tagged words? (Y/N): '):
        print('\nTagged words:', data_tagged_pos, sep='\n', end='\n\n')

    for (word, tag) in data_tagged_pos:
        temp_sent.append((word, tag))

        if tag == '.':
            data_sentences.append(temp_sent)
            temp_sent = []

    if yn_prompt('Would you like to '
                 'see the tagged words one sentence at a time? (Y/N): '):
        print('\nSentences:')

        curr_index, data_num_sentences = 0, len(data_sentences)
        valid_input = True

        while curr_index < data_num_sentences:
            if valid_input:
                print('\n', data_sentences[curr_index], sep='')
                valid_input = False

            while not valid_input:
                user_input = input('\n<enter> to see more, '
                                   '<S> to skip the rest: ')
                valid_input = is_valid_input(user_input, ('', 's', 'skip'))

            if user_input == '':
                curr_index += 1
            else:
                curr_index = data_num_sentences
                print('Skipped', end='\n\n')

    repeat = yn_prompt('Would you like to enter new data? (Y/N): ')

print('Goodbye!')
